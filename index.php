<?php include "functions.php"; ?>
<?php include "config.php"; ?>
<html>
<head>
    <title>Animal LatestNews</title>
    <?php include "libraries.php"; ?>
    <?php include "language.php"; ?>
</head>
<body>
    <div class="container">
        <?php include "parts/header.php"; ?>
        <div class="row row-eq-height" style="min-height: 800px;">
            <?php include "parts/news.php"; ?>
            <?php include "parts/sidebar.php"; ?>
        </div>
        <?php include "parts/footer.php"; ?>
    </div>
</body>
</html>
