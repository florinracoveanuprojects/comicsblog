<?php include "functions.php"; ?>
<?php include "config.php"; ?>
<html>
<head>
    <title>Animal LatestNews</title>
    <?php include "libraries.php"; ?>
    <?php include "language.php"; ?>
</head>
<body>
<div class="container">
    <?php include "parts/header.php"; ?>
    <div class="row row-eq-height">
        <div class="col-sm-9" style="background-color:white;">
            <?php
            global $mysqlConnect;
            $limit = 12;
            $newsOnRow = 4;
            if (isset($_GET['page'])) {
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            if(isset($_POST['search'])){
                $_SESSION['search'] = $_POST['search'];
                $result = mysqli_query($mysqlConnect, "SELECT * FROM news where language = '".$_SESSION['language']."' and (title like '%".$_SESSION['search']."%' or short_description like '%".$_SESSION['search']."%' or description like '%".$_SESSION['search']."%' or category like '%".$_SESSION['search']."%')");
                $numberOfNews = mysqli_num_rows($result);
                $totalPages = ceil($numberOfNews / $limit);
                $newsItems = $result->fetch_all(MYSQLI_ASSOC);
                foreach ($newsItems as $key => $newsItem) {
                    if (($key % $newsOnRow == 0) && ($key != 0)) { ?>
                        </div>
                        <div class="row"><?php
                    } ?>
                    <div class="col-sm-<?php echo 12 / $newsOnRow; ?> text-center">
                        <?php news($newsItem, $newsItem['key_number']); ?>
                    </div><?php
                }
                pagination($totalPages, $pageNumber);

                }else{
                    $result = mysqli_query($mysqlConnect, "SELECT * FROM news where language = '" . $_SESSION['language'] . "' and key_number = '" . $_GET['key'] . "'");
                    $newsItems = $result->fetch_all(MYSQLI_ASSOC);
                    newsDescription($newsItems[0]);?>
                                <h4><?php echo $writeAcomment; ?></h4>
                                <form action="" method="post">
                                <label for="usr"><?php echo $name; ?>:</label>
                                <input type="text" class="form-control" id="usr">
                                <label for="email">Email :</label>
                                <input type="text" class="form-control" id="email">
                                <label for="comment"><?php echo $comment; ?>:</label>
                                <textarea class="form-control" rows="5" id="comment"></textarea><br/>
                                <button type="submit" class="btn btn-default"><?php echo $post; ?></button>
                            </form><?php
                }?>
        </div>
        <?php include "parts/sidebar.php"; ?>
    </div>
    <?php include "parts/footer.php"; ?>
</div>
</body>
</html>


