<div class="col-sm-8 text-left">
    <div class="row">
        <div class="col-sm-6">
            <img src="images/golf.jpg" width="200">
            <div class="panel-footer" style="background-color: #737373; width:200">
                <a href="" style="color:white">Sport</a>
            </div>
        </div>
        <div class="col-sm-6">
            <p>Vazand ca nimeni nu reuseste sa inscrie, s-a hotarat sa ii ajute...<a href="">Citeste tot</a></p>
            <p style="color: #d9d9d9"><b>Publicat la....de...</b>
                <a href="#" style="float:right">
                    <span class="glyphicon glyphicon-envelope"></span>
                </a>
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-6">
            <img src="images/caineTopit.jpg" width="200">
            <div class="panel-footer" style="background-color: #737373; width:200">
                <a href="" style="color:white">Sport</a>
            </div>
        </div>
        <div class="col-sm-6">
            <p>Un caine s-a topit de pofta...<a href="">Citeste tot</a></p>
            <p style="color: #d9d9d9"><b>Publicat la....de...</b>
                <a href="#" style="float:right">
                    <span class="glyphicon glyphicon-envelope"></span>
                </a>
            </p>
        </div>
        <br/>
    </div>
    <div class="row">
        <div class="col-sm-6">

            <img src="images/lama.jpg" width="200">
            <div class="panel-footer" style="background-color: #737373; width:200">
                <a href="" style="color:white">Sport</a>
            </div>
        </div>
        <div class="col-sm-6">
            <p>Alpaca lui Godina si-a facut o schimbare de look...<a href="">Citeste tot</a></p>
            <p style="color: #d9d9d9"><b>Publicat la....de...</b>
                <a href="#" style="float:right">
                    <span class="glyphicon glyphicon-envelope"></span>
                </a>
            </p>
        </div>
        <br/>
    </div>
</div>